import { Router } from 'express';
import { UserRouter } from './user.router';
import { AddressRouter } from './address.router';
const router: Router = Router();

router.use('/user', UserRouter);
router.use('/address', AddressRouter);

export const IndexRouter: Router = router;
