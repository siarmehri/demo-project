import { Request, Response, Router } from 'express';
const router: Router = Router();

router.post("/", (req: Request, res: Response) => {
  const body = req.body;
  return res.send(body);
});


router.get("/:id", (req: Request, res: Response) => {
  const { id } = req.params;
  const user={
    name: "Siar",
    email: "siar@gmail.com",
    id
  }
  return res.send(user);
});

router.delete("/:id", (req: Request, res: Response) => {
  const {id} = req.params;
  return res.send({id});
});



export const UserRouter: Router = router;