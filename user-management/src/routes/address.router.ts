import { Request, Response, Router } from 'express';
import { IAddress } from '../interfaces/IAddress';
import { Address } from '../model/Address';
import { sequelize } from '../util/sequelize';
import { Transaction } from 'sequelize';

const router: Router = Router();

router.post("/", async (req: Request, res: Response) => {
  const address: IAddress = req.body;
  try {
    await sequelize.transaction(async (transaction: Transaction) => {
      return res.send(await Address.UpdateOrCreate(address, transaction));
    });
  } catch (error) {
    return res.status(500).send({ error });
  }
});


router.get("/:id", async (req: Request, res: Response) => {
  const { id } = req.params;
  try {
    await sequelize.transaction(async (transaction: Transaction) => {
      return res.send(await Address.FindOne(+id, transaction));
    });
  } catch (error) {
    return res.status(500).send({ error });
  }
});

router.delete("/:id", async (req: Request, res: Response) => {
  const { id } = req.params;
  try {
    const count = await sequelize.transaction(async (transaction: Transaction) => {
      return await Address.Delete(+id, transaction);
    });

    if (count > 0) {
      return res.send({ address_id: +id, action: "Deleted" });
    }

    return res.status(404).send({ message: `No record found for id: ${id}` });
  } catch (error) {
    return res.status(500).send({ error });
  }
});

export const AddressRouter: Router = router;