import bodyParser from "body-parser";
import express from "express";
import { IndexRouter } from "./routes/route.index";
import { DBConnection, sequelize } from "./util/sequelize";
import { Models } from "./model/model.index";
import { umzug } from "./util/umzug";
import { config } from "./config/config";
(async () => {
  /*
  const DBConnected: boolean = await DBConnection();
  if(!DBConnected) {
    console.log('could not connect to db, shutting down');
    process.exit(1);
  }
  sequelize.addModels(Models);
  const port = process.env.PORT || 8080; // default port to listen
  const app = express();
  try {
    // Migrations and Seeders
    await umzug.up();
    app.listen( port, () => {
      console.log( `server running localhost:${port}`);
      console.log( `press CTRL+C to stop server` );
    });
  } catch (err) {
    console.log(`Producer Service: Could not sync ${(err as any).message}`);
    process.exit(1);
  }
  app.use(bodyParser.json({ limit: "20mb" }));

  app.use(config.base_url, IndexRouter)

  app.get(config.base_url, (req, res) => {
    res.send({
      message: "404"
    });
  });*/
})();


