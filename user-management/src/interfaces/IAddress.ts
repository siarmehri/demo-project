export interface IAddress {
  type: string;
  address_line_1: string;
  country: string;
  post_code: string;
}