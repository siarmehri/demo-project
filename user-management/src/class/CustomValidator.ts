import { NextFunction, Request, Response } from "express";

type TypeOfPrimitive = "boolean" | "number" | "string";
type Primitives = boolean | number | string;

export class CustomValidator {
  public static IsValidNumber(
    param: number,
    name: string
  ) {
    if (typeof +param !== 'number' || isNaN(+param)) {
      throw new Error(`${name}: ${param} is not a valid number`);
    }
  }

  public static IsValidString(param: string, name: string, required: boolean = true) {
    if (required) {
      if (!param || typeof param !== 'string') {
        throw new Error(`${name} is not a valid string`);
      }
    }
    return true;
  }

  public static ObjectHasKeys(
    obj: object,
    fields: string[],
    objectName: string = "object"
  ) {
    const objKeys = Object.keys(obj);
    fields.forEach((f) => {
      if (!objKeys.includes(f)) {
        throw new Error(`${f} is required field in ${objectName}`);
      }
    });
  }
}
